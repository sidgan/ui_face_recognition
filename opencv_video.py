'''
uses the logitech webcam, reads the video, displays only the 96x96x3 crop of the face 
found by the bounding box and then calls the recognition code with the 
number of faces it finds and the bounding box dimensions.
'''
import cv2
import os
import time
import sys
from operator import itemgetter

#-1 for external camera
vc = cv2.VideoCapture(0)

if vc.isOpened(): # try to get the first frame
    rval, frame = vc.read()
else:
    rval = False

def write_bb_on_image(image):
	face_cascade_path = './haarcascades/haarcascade_frontalface_default.xml'
	face_cascade = cv2.CascadeClassifier(face_cascade_path)
	scale_factor = 1.1
	min_neighbors = 3
	min_size = (30, 30)
	outfname = ""
	flags = cv2.CASCADE_SCALE_IMAGE
	faces = face_cascade.detectMultiScale(image, scaleFactor = scale_factor, minNeighbors = min_neighbors, \
		minSize = min_size, flags = flags)
	#( x, y, w, h ) in faces
	return faces


#preprocess for recognition
def grab_face(x,y,w,h,frame):
	#a is array of size 96x96x3
	#a = 0
	a = frame[y: y + h, x: x + w]
	#reshape to 96x96x3
	resized_image = cv2.resize(a, (96, 96))
	return resized_image


#RECOGNIZE ONLY ONE PERSON

while rval:
	rgbImg = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
	bbs = write_bb_on_image(rgbImg)
	for (x, y, w, h ) in bbs:
		#print x,y,w,h
		#this is the bounding box
		cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 255, 0), 2)
		#get the 96 x 96 x 3 face 
		correct_size_face = grab_face(x,y,w,h,frame)
		cv2.imshow('see this', correct_size_face)
		#send the 96 x 96 x 3 face to the processor 
		#recognized_name = recognize_person(correct_size_face)
	#cv2.imshow("OpenCV", frame)
	rval, frame = vc.read()
	key = cv2.waitKey(20)
	if key == 27: # exit on ESC
		break


cv2.destroyWindow("OpenCV")