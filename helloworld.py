#!/usr/bin/env python

# example helloworld.py

import pygtk
pygtk.require('2.0')
import gtk
import detection
import filechooser


class HelloWorld:
    def call_image_load(self,widget,image):
        path = filechooser.show()
        print path
        image.set_from_file(path)


    def call_bounding_box(self,widget,data,image):
        print "call bounding box on this"
        #call opencv image bounding box code
        bb_file_name = detection.write_bb_on_image(data)
        image.set_from_file(bb_file_name)




    def call_recognition(self,widget,data):
        print data,"call recognition on this"
        #subprocess.Popen(["python", "helloworld.py"])

    def close_application(self, widget, event, data=None):
        gtk.main_quit()
        return False

    def delete_event(self, widget, event, data=None):
        print "delete event occurred"
        return True

    def destroy(self, widget, data=None):
        print "destroy signal occurred"
        gtk.main_quit()

    def __init__(self):
        # create a new window
        self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        self.window.set_title('Face recognition')
        #self.window.set_size_request(400,400)
        self.window.connect("delete_event", self.close_application)
        self.window.connect("destroy", self.destroy)
        self.window.set_border_width(10)


        #table
        self.table = gtk.Table(rows=1,columns=3, homogeneous=False)
        self.window.add(self.table)


        self.path = ''
    
        #ONE BUTTON   
        self.image = gtk.Image()
        self.image.set_from_file("./click_to_load.png")
        self.image.show()
        self.button = gtk.Button()
        self.button.add(self.image)
        self.button.connect("clicked", self.call_image_load,self.image)
        self.table.attach(self.button, 0,1,0,1)
        self.button.show()


        #SECOND BUTTON   
        self.image1 = gtk.Image()
        self.image1.set_from_file("./detect.png")
        self.image1.show()
        self.button1 = gtk.Button()
        self.button1.add(self.image1)
        self.button1.connect("clicked", self.call_bounding_box,'./images/obama.jpg',self.image1)
        self.table.attach(self.button1, 1,2,0,1)
        self.button1.show()


        #THIRD BUTTON   
        self.image2 = gtk.Image()
        self.image2.set_from_file("./recognize.png")
        self.image2.show()
        self.button2 = gtk.Button()
        self.button2.add(self.image2)
        self.button2.connect("clicked", self.call_recognition, "third")
        self.table.attach(self.button2, 2,3,0,1)
        self.button2.show()


        # and the window
        self.table.show()
        self.window.show()

    def main(self):
        # All PyGTK applications must have a gtk.main(). Control ends here
        # and waits for an event to occur (like a key press or mouse event).
        gtk.main()

# If the program is run directly or passed as an argument to the python
# interpreter then create a HelloWorld instance and show it
if __name__ == "__main__":
    hello = HelloWorld()
    hello.main()
