## how to run

**python helloworld.py**

This will open a window with 3 buttons. First button loads the image, second button calls the opencv face detector on the image, third button calls the face recognition process on the image


**python opencv_video.py** 

uses the logitech webcam, reads the video, displays only the 96x96x3 crop of the face found by the bounding box and then calls the recognition code with the number of faces it finds and the bounding box dimensions.


**python demo_pipeline.py**

takes images from a directory, displays the image, calls the opencv detection code, displays the bounding box, resizes to 96x96x3 and sends it to the recognition code. However, this isnt fully automatic. It needs some kind of user input to make the pipeline progress.