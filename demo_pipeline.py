'''
takes images from a directory, displays the image, calls the opencv detection code, 
displays the bounding box, resizes to 96x96x3 and sends it to the recognition code.
However, this isnt fully automatic. 
It needs some kind of user input to make the pipeline progress.
'''
import cv2
import os
import sys
import os.path


#find all images in folder
def find_all_images(path):
	imgs = []
	if not path: 
		path = "./images/"
	valid_images = [".jpg",".png",".jpeg"]
	for f in os.listdir(path):
		ext = os.path.splitext(f)[1]
		if ext.lower() not in valid_images:
			continue
		imgs.append(os.path.join(path,f))
	return imgs

#get detection results
def write_bb_on_image(image_path):
	print image_path
	face_cascade_path = './haarcascades/haarcascade_frontalface_default.xml'
	face_cascade = cv2.CascadeClassifier(face_cascade_path)
	output_bb_path = "./bb"
	scale_factor = 1.1
	min_neighbors = 3
	min_size = (30, 30)
	outfname = ""
	flags = cv2.CASCADE_SCALE_IMAGE
	image = cv2.imread(image_path)
	faces = face_cascade.detectMultiScale(image, scaleFactor = scale_factor, minNeighbors = min_neighbors, \
		minSize = min_size, flags = flags)
	for( x, y, w, h ) in faces:
		cv2.rectangle(image, (x, y), (x + w, y + h), (255, 255, 0), 2)
		outfname = output_bb_path+"/%s.faces.jpg" % os.path.basename(image_path)
		cv2.imwrite(os.path.expanduser(outfname), image)
	results = [x,y,w,h,outfname]
	return results

#preprocess for recognition
def grab_face(x,y,w,h,frame):
	#a is array of size 96x96x3
	#a = 0
	a = frame[y: y + h, x: x + w]
	#reshape to 96x96x3
	resized_image = cv2.resize(a, (96, 96))
	return resized_image


def main():
	path = "./images/"
	#find all images in folder
	image_paths = find_all_images(path)
	print image_paths
	for image_path in image_paths:
		#read image
		print image_path
		image = cv2.imread(image_path)
		#display image
		cv2.imshow('see this',image)
		#wait key units in milli seconds
		# 0 wont close until the user presses any key
		cv2.waitKey(0)
		#send to detection
		#get detection results
		[x,y,w,h,outfname] = write_bb_on_image(image_path)
		print [x,y,w,h,outfname]
		#display detection results
		detection_image = cv2.imread(outfname)
		cv2.imshow('see this',detection_image)
		#wait key units in milli seconds
		# 0 wont close until the user presses any key
		cv2.waitKey(0)
		#preprocess for recognition
		processed_image = grab_face(x,y,w,h,image)
		#assert size of processed_image is 96x96x3
		height, width, channels = processed_image.shape
		print height, width, channels
		assert height == width == 96
		assert channels == 3
		cv2.imshow('see this',processed_image)
		#wait key units in milli seconds
		# 0 wont close until the user presses any key
		cv2.waitKey(0)
		#send to recognition 
		#get recognition results
		#display recognition results
		#repeat
	


if __name__ == '__main__':
	main()