import cv2
import os
import sys
from string import Template

print 'det'

def write_bb_on_image(image_path):
	print image_path
	face_cascade_path = '/home/sidgan/Downloads/demo-socionext/haarcascades/haarcascade_frontalface_default.xml'
	face_cascade = cv2.CascadeClassifier(face_cascade_path)
	output_bb_path = "/home/sidgan/Downloads/demo-socionext/bb"
	scale_factor = 1.1
	min_neighbors = 3
	min_size = (30, 30)
	outfname = ""
	flags = cv2.CASCADE_SCALE_IMAGE
	image = cv2.imread(image_path)
	faces = face_cascade.detectMultiScale(image, scaleFactor = scale_factor, minNeighbors = min_neighbors, \
		minSize = min_size, flags = flags)
	for( x, y, w, h ) in faces:
		cv2.rectangle(image, (x, y), (x + w, y + h), (255, 255, 0), 2)
		outfname = output_bb_path+"/%s.faces.jpg" % os.path.basename(image_path)
		cv2.imwrite(os.path.expanduser(outfname), image)
	return outfname